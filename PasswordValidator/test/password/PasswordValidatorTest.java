package password;

import static org.junit.Assert.*;
import org.junit.Test;

/*
 * @author Timothy Marasigan 991321050
 * 
 * This program tests the methods in the PasswordValidator class.
 * 
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {		
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("fdfdRfff"));		
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {		
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));		
	}


	@Test
	public void testHasValidCaseCharsException() {		
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));		
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {		
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));		
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {		
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));		
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {		
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));		
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {		
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));		
	}
	
	////////

	@Test
	public void testIsValidLengthRegular() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloWorld");
		assertTrue("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello");
		assertFalse("Invalid Password Length", isValidLength);		
	}
	
	@Test
	public void testIsValidLengthExceptionSpace() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello World");
		assertFalse("Invalid Password Length", isValidLength);		
	}
	
	@Test
	public void testIsValidLengthExceptionNull() {
		boolean isValidLength = PasswordValidator.isValidLength(null);
		assertFalse("Invalid Password Length", isValidLength);		
	}

	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello1");
		assertFalse("Invalid Password Length", isValidLength);		
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello123");	
		assertTrue("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testHasValidDigitCountRegular() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Pass123");
		assertTrue("Invalid Password Length", hasValidDigitCount);		
	}
	
	@Test
	public void testHasValidDigitCountException() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Pass1");
		assertFalse("Invalid Password Length", hasValidDigitCount);		
	}
	
	@Test
	public void testHasValidDigitCountBoundaryOut() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Pass");
		assertFalse("Invalid Password Length", hasValidDigitCount);		
	}
	
	@Test
	public void testHasValidDigitCountBoundaryIn() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Pass12");
		assertTrue("Invalid Password Length", hasValidDigitCount);
	}
	
}








