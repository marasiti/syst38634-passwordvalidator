package password;

/*
 * @author Timothy Marasigan 991321050
 * 
 * This program checks if a password is of valid length and contains enough digits.
 * 
 * It is assumed that spaces should be not be considered as valid characters for the purpose 
 * 
 * This is the new changes
 */

public class PasswordValidator {

	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;

	public static boolean isValidLength(String password) {
		return(password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		int numOfDigits = 0;
		
		for(int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i))) {
				numOfDigits++;
			}
		}
		
		return (numOfDigits >= MIN_DIGITS);
	}
	
}
